```js
// macro - I/O
input.onkeyup = console.log

console.log(1);

// timers
setTimeout(()=> console.log(2), 0)

// microtasks
Promise.resolve(3).then(res => console.log(res))

console.log(4);

now = Date.now()

// Blocking code
while(now + 5000 > Date.now()){  } 

console.log(5);

// Output
1
4
// After 5 sec...
5
3
KeyboardEvent {isTrusted: true, key: "a", code: "KeyA", location: 0, ctrlKey: false, …}
2