```js

function idMaker(max = 10) {
    let index = 0;
    return {
        next: function () {
            if(index === max) { return {value: index, done:true } };

            return {
                value: index++,
                done: false
            };
        }
    };
}
id = idMaker()

do{
    result = id.next()
    console.log(result.value)
    if(result.value > 5) { break; }

}while(result.done === false)

```
## Generator
```js
function* idMaker(max = 10) {
    let index = 0;

    while(index < max){
        yield index ++
    }
    return index;
}

for(let i of idMaker(Infinity)){ 
    console.log(i)
    if(i > 5) { break; }
} 
```


[Symbol.iterator]

```js
function idMaker(max = 10) {
    let index = 0;
    return {
        [Symbol.iterator]: function() { 
            return{
                next: function () {
                    if(index === max) { return {value: index, done:true } };

                    return {
                        value: index++,
                        done: false
                    };
                }
            }
        }
    };
}

for(let i of idMaker(Infinity)){ 
    console.log(i)
    if(i > 5) { break; }
} 

```

```js
function delay(value, delay = 100){
    return new Promise(resolve => setTimeout(() => resolve(value),delay) );
}

function idMaker(max = 10) {
    let index = 0;
    return {
        [Symbol.asyncIterator]: function() { 
            return {
                next: function () {
                    if(index === max) { return delay({value: index, done:true }) };

                    return delay({
                        value: index++,
                        done: false
                    });
                }
            }
        }
    };
}

i = 0;
handler = setInterval(() => console.log(i++), 100);
for await(let i of idMaker(Infinity)){ 
    console.log('Iterator ' + i)
    if(i > 5) { break; }
} 
clearInterval(handler);
```

## Async Generator
```js
function delay(value, delay = 100){
    return new Promise(resolve => setTimeout(() => resolve(value),delay) );
}

function* idMaker(max = 10) {
    let index = 0;

    while(index < max){
        yield delay(index ++)
    }
    return index;
}

i = 0;
handler = setInterval(() => console.log(i++), 100);
for await(let i of idMaker(Infinity)){ 
    console.log('Iterator ' + i)
    if(i > 5) { break; }
} 
clearInterval(handler);
```