// Named
// import {chmod, fchown} from 'fs'

// Named + rename
// import {chmod as changeMode, fchown} from 'fs'

// Default
// import fs from 'fs'

// All named
// import * as fs from 'fs'


var secret = 'I am top secret!'

export function calculateResult () {
    // require('dynamic import ' + someParam )
    // import('dynamic import ' + someParam)
    return 'calculated result \n '
}

export default calculateResult