import * as fs from 'fs'
import { access } from 'fs'

// window.alert()

process.env.HOST

// Cannot find name 'process'. 
// Do you need to install type definitions for node? 
// Try `npm i --save-dev @types/node`. 


// window.alert()
export { }

interface SomeType {
    value: string
}

// var x = '123';
// var x = 123;
var x: any = '123';

// function sum(a:string, b:string) /* : string */{
function sum(a: number, b: number) /* : string */ {
    return a + b
}

// [].find(p => p)

// Strict! No Implicit Any!
const totals = (params: any) => {
    return params;
}

export default sum;

// console.log(sum('Ala ', 'ma kota').toPrecision(3))
console.log(sum(x, 5).toPrecision(3))