import { ErrorRequestHandler } from "express"
import StatusCodes from 'http-status-codes'

export class DomainError extends Error {
    status = 500

    constructor(message: string = 'Unhandled error') {
        super(message)
        this.message = message;
    }

    toJSON() {
        return {
            error: {
                message: this.message,
                ...(process.env.NODE_ENV !== 'production' && {
                    stack: this.stack
                }),
                status: this.status,
            },
        }
    }
}

export class InvalidRequestError extends DomainError {
    constructor(message = 'Not Found') { super(message) }
    status = StatusCodes.BAD_REQUEST
}

export class NotFoundError extends DomainError {
    constructor(message = 'Not Found') { super(message) }
    status = 404
}

export class UnauthorizedError extends DomainError {
    constructor(message = 'Unauthorized') { super(message) }
    status = StatusCodes.UNAUTHORIZED
}

// Error handler middleware
export const myErrorHandler: ErrorRequestHandler = (error, req, res, next) => {

    if (error instanceof DomainError) {
        res.status(error.status).json(error.toJSON())
    } else {
        res.status(500).json({
            message: error.message
        })
    }
}