import { RequestHandler } from "express"
import Router from "express-promise-router"
import { InvalidRequestError } from "../common/errors"
import { LoginDTO, loginUser, registerUser, User } from "../services/users"
import { } from 'passport-local'
import passport from "passport"
declare module 'express-session' {
    interface SessionData {
        views: number;
        user: any,
        rodo: boolean
    }
}

export const authRoutes = Router()


authRoutes.get('/login', async (req, res) => {
    res.render('login', {})
})
authRoutes.post<{}, User, LoginDTO, {}>('/login',
    // // JSON API:
    // passport.authenticate("local", { failureRedirect: '/login', successRedirect: '/pages' })
    // HTML Form:
    async (req, res, next) => {
        passport.authenticate("local", {}, (err, user, error) => {

            if (user) {
                req.logIn(user, () => {
                    return res.redirect('/pages')
                })
            }

            let userPayload = req.body || {}

            res.render('login', {
                user: userPayload,
                message: error?.message
            })

        })(req, res, next)
    })

authRoutes.use('/register', async (req, res) => {
    let user = req.body || {}
    let message = ''
    /* if (req.method === 'GET') {} else */

    if (req.method === 'POST') {
        try {
            await registerUser(user)
            await loginUser(user)
            res.redirect('/pages')
        }
        catch (error) {
            message = error.message
        }
    }

    res.render('register', {
        user,
        message
    })
})

authRoutes.get('/', function (req, res, next) {
    if (req.session.views) {
        req.session.views++
        res.send('Welcome back! Its your' + req.session.views + ' visit!')
    } else {
        req.session.views = 1
        res.send('Hello! Its your first time with us')
    }
})

authRoutes.get('/logout', (req, res) => {
    // req.isAuthenticated()
    // req.isUnauthenticated()
    // req.login()
    req.logOut()
    res.redirect('/pages')
})
// /auth/register


// /auth/login


export function myUserMiddleware(): RequestHandler {
    return (req, res, next) => {
        // res.locals.user = req.session.user || {
        //     username: 'Guest'
        // }

        res.locals.user = req.user || { username: 'Guest' }
        res.locals.title = 'Pages'
        next()
    }
}