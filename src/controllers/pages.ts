import express, { } from "express";
import multer from "multer";
import path from "path";
import fs from "fs";
import { createProduct, findProducts, updateProduct } from "../services/products";
import { UnauthorizedError } from "../common/errors";
import Router from 'express-promise-router';
export const pagesRoutes = Router()

pagesRoutes.get('/', (req, res) => {
    const title = 'Pages'

    const pages = [
        { title: 'Products', url: '/pages/products' },
        { title: 'Cart', url: '/pages/cart' },
        { title: 'About', url: '/pages/about' },
    ]

    res.render('pages', {
        title,
        pages
    })
})

pagesRoutes.get('/products', async (req, res) => {
    const filter = req.query.filter
    const products = await findProducts(String(filter || ''))

    res.render('products', {
        products,
        filter
    })
})

pagesRoutes.get('/product-form', (req, res) => {
    res.render('product-form', {
        product: { name: '', price: 0, description: '' }
    })
})

const uploader = multer({
    dest: path.resolve(__dirname, '../../data/uploads/'),
    // storage: amazons3storage?
})

pagesRoutes.post('/product-form',
    uploader.fields([
        { name: 'image', maxCount: 1 }
    ]), async (req, res) => {
        const { name, price, description } = req.body

        if (!req.user) {
            throw new UnauthorizedError('Must be logged in to create products')
        }

        let product = await createProduct({ name, price, description })
        debugger
        if ('image' in req.files) {
            const { destination, path: uploadedPath, filename, originalname } = req.files['image'][0]

            const productRelativePath = path.join('products', product._id, 'images')
            const productImageFileName = product._id + path.extname(originalname)
            const productImagesPath = path.resolve(__dirname, '..', '..', 'public', productRelativePath);
            const imagePath = path.resolve(productImagesPath, productImageFileName);

            await fs.promises.mkdir(productImagesPath, { recursive: true })
            await fs.promises.rename(uploadedPath, imagePath)
            product.image = path.join('/', productRelativePath, productImageFileName);
        }

        product = await updateProduct(product._id, product)

        // const product = ({ name, price, description, image })

        res.render('product-form', {
            product
        })
    })

// res.write('asd')
// res.end()
// res.json([])
// res.download()
// res.sendFile()
// res.format({ 'text/html': () => res.send('..html..') })
// res.send(`<html>
//     <h1>Pages</h1>
// </html>`)
