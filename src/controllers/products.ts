import { createProduct, deleteProduct, findProducts, getProductById, updateProduct } from "../services/products";

// Replace:
// import { Router } from "express";
// With:
import Router from 'express-promise-router'
import { Product } from "../models/product";

export const productsRoutes = Router()


// /products?q=123
productsRoutes.get('/', async (req, res) => {
    const search = String(req.query['q'] || '')

    if (search) {
        res.send(await findProducts(search))
    } else {
        res.send(await findProducts())
    }
})


productsRoutes.get('/init', (req, res) => {

    Product.insertMany([
        { name: 'Product 123', description: '', price: 100 },
        { name: 'Product 234', description: '', price: 200 },
        { name: 'Product 345', description: '', price: 300 },
    ])
})

// /products/123
productsRoutes.get('/:product_id', async (req, res, next) => {
    const product_id = req.params['product_id']
    const product = await getProductById(product_id);

    res.send(product)
})

productsRoutes.delete('/:product_id', async (req, res, next) => {
    const product_id = req.params['product_id']
    const product = await deleteProduct(product_id);

    res.send({ status: 'OK' })
})

productsRoutes.put('/:product_id', async (req, res, next) => {
    const product_id = req.params['product_id']
    const productPayload = req.body
    const product = await updateProduct(product_id, productPayload)

    res.send(product)
})

productsRoutes.patch('/:product_id', async (req, res, next) => {
    const product_id = req.params['product_id']
    const productPayload = req.body
    const product = await updateProduct(product_id, productPayload)

    res.send(product)
})

productsRoutes.post('/', async (req, res) => {
    const productPayload = req.body
    const product = await createProduct(productPayload)

    res.send(product)
})
