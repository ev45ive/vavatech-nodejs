import Router from "express-promise-router";
import { findUsers } from "../services/users";


export const usersRotues = Router()

usersRotues.get('/', async (req, res) => {
    const users = await findUsers()

    res.json(users)
})