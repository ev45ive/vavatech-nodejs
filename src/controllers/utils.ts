import EventEmitter from "events"
import { Router } from "express"
import path from "path"
import { Stream } from "stream"
import { generateFakeLogsGenerator } from "../lib/log-generator-iterator"
import { md5 } from "../lib/md5"

export const utilRoutes = Router()

utilRoutes.get('/streams', (req, res) => {
    // const readStream = fs.createReadStream(path.resolve(__dirname, '../data/example.txt'), {
    //     highWaterMark: 10,
    //     encoding: 'utf-8'
    // })
    const readStream = new Stream.Readable({
        read(n: number) {
            console.log('read ' + n + ' bytes')
            this.push('hi!')
            this.push('ho!')
            this.push(null)
        }
    })
    // readStream.on('data', console.log)
    // readStream.on('end', console.log)
    readStream.pipe(process.stdout)

    // readStream.on('data', data => res.emit('data', data))
    // readStream.on('data', data => res.write(String(data).toUpperCase()))
    // readStream.on('end', (data: any) => res.end(data))

    readStream.pipe(res)

})


utilRoutes.get('/md5', async (req, res) => {
    try {
        const filePath = path.resolve(__dirname, '../../data/example.txt')
        const resultMd5 = await md5(filePath)
        res.send(resultMd5)
    } catch (err) { res.status(500).send(err.message) }
})


utilRoutes.get('/events', (req, res) => {
    res.setHeader('Content-Type', 'text/html')
    res.flushHeaders()

    const emitter = new EventEmitter({})

    // req.app.emit('some global event')
    // req.app.on('some global event', console.log)

    // Write to HTTP client (browser)
    const sendData = (data: any) => { res.write(data) }
    emitter.on('data', sendData)
    emitter.on('end', (data) => {
        res.end(data)
        emitter.off('data', sendData)
    })

    // Write to console
    emitter.on('data', (data) => { console.log(data) })
    emitter.on('end', (data) => { console.log(data) })

    // startProcess(emitter)
    emitter.on('start', () => {
        setTimeout(() => emitter.emit('data', '<h1>Hello events!</h1>'), 1000);
        setTimeout(() => emitter.emit('data', '<h2>Hello Event Emitters!</h2>'), 2000);
        setTimeout(() => emitter.emit('end', '<h3>No more events!</h3>'), 5000);
        setTimeout(() => emitter.emit('data', '<h2>Data after end!?!</h2>'), 6000);
    })
    emitter.emit('start')
})

utilRoutes.get('/blocking', (req, res) => {
    for (let i = 0; i < 10_000; i++) {
        let now = Date.now()
        while (now + 1000 > Date.now()) { }
        res.write(`<b>Blocking</b>`)
    }
    res.end()
})

utilRoutes.get('/nonblocking', (req, res) => {
    let i = 0;
    const handle = setInterval(() => {
        if (++i > 10_000) {
            res.end();
            clearInterval(handle)
        }
        res.write(`<b>Non Blocking</b> <br/>`)
    }, 1000)
})

utilRoutes.get('/logs', async (req, res) => {
    try {
        const n = Number(req.query.n) || 100
        res.setHeader('Content-Type', 'text/html')
        res.flushHeaders()

        const file = '../../logs/server/serverlog1.txt'
        const filePath = path.resolve(__dirname, file)
        const result = generateFakeLogsGenerator(filePath, n)

        res.write('<h1>Started generator 1</h1>')

        for await (let log of result) {
            res.write('<p>Generated log ' + log)
        }

        res.end('<h3>Generated  logs</h3>')
    } catch (err) {
        res.end('Error ' + err)
    } finally { /* after then or error */ }
})