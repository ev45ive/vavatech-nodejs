import express, { } from 'express'
import { myErrorHandler } from './common/errors'
import * as controllers from './controllers'
import { configureMiddleware } from './config/config'
import mongoose from 'mongoose'
import { Product } from './models/product'
import { MONGO_AUTH, MONGO_URL } from './config'

const app = express()
configureMiddleware(app)

app.get('/', (req, res) => {
    res.send(`<h1>Hello Express App</h1>`)
})
app.use('/auth', controllers.authRoutes)
app.use('/utils', controllers.utilRoutes)
app.use('/products', controllers.productsRoutes)
app.use('/pages', controllers.pagesRoutes)
app.use('/cart', controllers.cartRoutes)
app.use('/users', controllers.usersRotues)

app.use(myErrorHandler)

const PORT = Number(process.env.PORT || 8080)
const HOST = process.env.HOST || 'localhost'

mongoose.connect(MONGO_URL, {
    // auth: MONGO_AUTH,
    useNewUrlParser: true,
    useUnifiedTopology: true
}, (err) => {
    if (err) { console.error(err) }
    app.emit('database_ready');

    app.listen(PORT, HOST, () => {
        console.log(`Listening on http://${HOST}:${PORT}/`)
    })
})


app.on('database_ready', () => {

})