import fs from 'fs'
import path from 'path'


export async function generateFakeLogsAsyncAwait(filePath: string, n = 1000) {

    await fs.promises.mkdir(path.dirname(filePath), { recursive: true })

    for (var i = 0; i < n; i++) {
        const log = (`[${(new Date()).toISOString()}] [Info] - Example log ${~~(Math.random() * 10000)} \n`)

        await fs.promises.appendFile(filePath, log)
    }

    return i;
}