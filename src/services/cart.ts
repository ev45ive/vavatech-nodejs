import { getProductById, Product } from "./products"

interface CartItem {
    product: Product
    count: number
}

interface Cart {
    items: CartItem[];
    count: number;
    total: number;
}

const cart: Cart = {
    items: [],
    count: 0,
    total: 0
}

export async function addToCart(product_id: string) {
    const index = cart.items.findIndex(i => i.product._id === product_id)
    const product = await getProductById(product_id)
    
    if (index === -1) {
        cart.items.push({
            count: 1,
            product
        })
    } else {
        cart.items[index].count++
    }
    cart.count = cart.items.reduce((count, item) => count + item.count, 0)
    cart.total = cart.items.reduce((total, item) => total + item.count * item.product.price, 0)
}

export const getCart = async () => {
    return cart
}