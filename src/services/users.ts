export interface User {
    id: string;
    username: string;
    password: string;
}
interface CreateUserDTO {
    username: string;
    password: string;
}
export type LoginDTO = CreateUserDTO

import { v4 as uuid } from 'uuid'
import { NotFoundError, UnauthorizedError } from '../common/errors';

const usersData: User[] = [
    { id: '123', username: 'admin', password: 'admin' }
]

export const findUsers = async () => {
    return usersData;
}

export const getUserById = async (id: string) => {
    return usersData.find(u => u.id === id)
}

export const registerUser = async (data: CreateUserDTO) => {
    const user = { id: uuid(), ...data }
    usersData.push(user)
    return user
}

export const loginUser = async (data: LoginDTO) => {
    const user = usersData.find(u => u.username === data.username)

    if (!user) { throw new NotFoundError('User does not exit') }
    if (user.password !== data.password) { throw new UnauthorizedError('Password does not match') }

    return user
}