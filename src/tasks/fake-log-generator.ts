// global // equivalent of browser 'window' - top level scope
import fs from 'fs'
import path from 'path'
import { generateFakeLogs } from '../lib/log-generator'
import minimist from 'minimist'

const params = minimist(process.argv.slice(2))
// const file = process.argv.slice(2)[0]
// const count = process.argv.slice(3)[0]

console.log(params)
const { _: [file], n: count } = params
console.log(file, count)

const filePath = path.resolve(process.cwd(), file)
// ts-node src/tasks/fake-log-generator.ts ../vavatech-nodejs/logs/fakelog.txt
// C:\Projects\szkolenia\vavatech-nodejs\logs\fakelog.txt
console.log(filePath)

generateFakeLogs(filePath, count, () => { })

// global;
// global.process

// console.log(__dirname)
// console.log(__filename)
// console.log(process.cwd());
// process.chdir(__dirname)
// console.log(process.cwd());

//  npx cross-env PORT=9000 ts-node src/tasks/fake-log-generator.ts a.txt b.txt
// console.log(process.execPath);
// console.log(process.execArgv);
// console.log(process.argv);
// console.log(process.argv.slice(2));

// console.log(process.env.PORT);
// console.log(process.pid);
// process.kill(4756)
// npx cross-env PORT=9000 ts-node src/tasks/fake-log-generator.ts a.txt b.txt && echo 'placki'
// process.exit(1)
// process.exit(0)

// process.stdout.write('Write to stream')
// console.log(process.stdin.read(10))

// const readline = require('readline');
// const rl = readline.createInterface({
//     input: process.stdin,
//     output: process.stdout,
//     // completer(linePartial, callback) { callback(null, [['123'], linePartial]); })
// });
// rl.prompt();
// rl.on('line', (line: string) => { console.log(`Received: ${line}`); });
// // or
// rl.question('What is your ultimate question? ', (answer: string) => {
//     console.log(`42`);
//     rl.close();
// });

// __dirname // current directory
// __filename // current file name

// // method for importing exports from modules
// require('./othermodule')
// // CommonJS module exports
// module.exports = { something: 123}
// exports.something = 123
// process // current system process information
// // Text / URL tools
// TextDecoder TextEncoder URL URLSearchParams
// // Async Event Loop tools
// queueMicrotask(callback), setImmediate(callback),
// setInterval(callback,