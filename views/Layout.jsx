import React from 'react'

export const Layout = (props) => {
    return (
        <html lang="en">
            <head>
                <meta charSet="UTF-8" />
                <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>Document</title>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
                    integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossOrigin="anonymous" />
            </head>

            <body>
                <div id="root">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <img src="/logo.svg" width="100" className="float-end"  />
                                <h3>Welcome, {props.user.username}!</h3>

                                <a href="/auth/login">Login</a>
                            </div>
                        </div>
                    </div>
                    {props.children}
                </div>
                <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
                    integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
                    crossOrigin="anonymous"></script>
            </body>
        </html>
    )
}

export default Layout