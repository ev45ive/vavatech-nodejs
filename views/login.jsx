import React from 'react'
import Layout from './Layout.jsx';

export const Pages = (props) => {
    return (
        <Layout {...props}>
            <div className="container">
                <div className="row">
                    <div className="col">

                        <form action="/auth/login" method="POST">

                            {props.message && <p className="alert alert-danger">{props.message}</p>}

                            <div className="form-group mb-3">
                                <input type="text" className="form-control" name="username" defaultValue={props.user.username} />
                            </div>

                            <div className="form-group mb-3">
                                <input type="password" className="form-control" name="password" defaultValue={props.user.password} />
                            </div>

                            <button className="btn btn-success" type="submit">Login</button>
                        </form>

                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default Pages;