import React from 'react'
import Layout from './Layout.jsx';

export const Pages = (props) => {
    return (
        <Layout {...props}>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <form action="/pages/product-form" method="POST" encType="multipart/form-data">
                            <div className="form-group">
                                <label htmlFor="">Name:</label>
                                <input type="text" className="form-control" name="name" defaultValue={props.product.name} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Price:</label>
                                <input type="number" className="form-control" name="price" defaultValue={props.product.price} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Description:</label>
                                <textarea className="form-control" name="description" defaultValue={props.product.description} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Product image:</label>
                                <input type="file" className="form-control" name="image" />
                            </div>
                            <a href="/pages/products" className="btn btn-danger mt-3">Cancel</a>
                            <button className="btn btn-success float-end mt-3">Save</button>
                        </form>
                    </div>

                </div>
            </div>
        </Layout>
    )
}

export default Pages;